<?php
/**
 * @file
 * Plugin include file for export style plugin.
 */

include('views_data_export_plugin_style_export_fasta.inc');

/**
 * Generalized style plugin for export plugins.
 *
 * @ingroup views_style_plugins
 */
class views_data_export_plugin_style_export_kaspar extends views_data_export_plugin_style_export_fasta {

  /**
   * The init hook -called when the plugin is initialized
   * A good place to add global variables
   */
  function init (&$view, $options) {
    parent::init($view, $options);

    // Adds a global variable with the IUPAC codes needed to change A/G into R
    // Used for any surrounding SNPs
    if (!isset($this->IUPAC)) {
      $this->IUPAC = array(

        'AG' => 'R',
        'GA' => 'R',

        'CT' => 'Y',
        'TC' => 'Y',

        'GC' => 'S',
        'CG' => 'S',

        'AT' => 'W',
        'TA' => 'W',

        'GT' => 'K',
        'TG' => 'K',

        'AC' => 'M',
        'CA' => 'M',

        'CGT' => 'B',
        'GCT' => 'B',
        'TGC' => 'B',
        'TCG' => 'B',

        'AGT' => 'D',
        'GAT' => 'D',
        'TGA' => 'D',
        'TAG' => 'D',

        'ACT' => 'H',
        'CAT' => 'H',
        'TCA' => 'H',
        'TAC' => 'H',

        'ACG' => 'V',
        'CAG' => 'V',
        'GAC' => 'V',
        'GCA' => 'V',
      );
    }
  }

  /**
   * Options form mini callback.
   * This is where you add options that can be selected by the views admin
   * when a KASPar export is added
   *
   * @param $form
   * Form array to add additional fields to.
   * @param $form_state
   * State of the form.
   * @return
   * None.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

  	$form ['featuredfn'] = array(
  		'#type' => 'fieldset',
  		'#title' => 'Features'
  	);

  	$form['featuredfn']['description'] = array(
  		'#type' => 'item',
  		'#value' => 'The following options allow you to describe the layout of your data to this plugin '
  			.'in order for genotypes of markers to be pulled in. This plugin assumes that you have '
  			.'parental features (ie: contigs) to which loci are aligned. There are then markers related '
  			.'to a given locus which have genotypes attached to them. The features to be output should be '
  			.'either loci or markers and should be indicated using the "Feature ID Column" select box.'
  	);

  	// Sets what type of features are in the parent view so that the plugin knows what
  	// related features need to be looked up
  	$form['featuredfn']['feature_type'] = array(
  		'#title' => 'Feature Column Type',
  		'#type' => 'radios',
  		'#options' => array(
  			'contig' => 'Backbone: Feature upon which loci are located, usually has associated '
  			  .'sequence (ie: pseduomolecule, contig)',
  			'locus' => 'Locus: Feature indicating a point of interest, usually related to a marker',
  			'marker' => 'Marker: Feature with associated genotypes',
  		),
  		'#description' => 'Choose the Type of Features in the Feature ID Column. Depending upon what '
  		  .'is selected here, the remaining two related features will be looked up.',
  		'#default_value' => $this->options['featuredfn']['feature_type'],
  	);

    // Sets the controlled vocabulry term used in the feature_relationship table to link
    // a locus to its marker
		$types = array();
  	$results = tripal_core_chado_select('cvterm',array('cvterm_id','name'),array('cv_id'=>array('name'=>'relationship')));
  	foreach ($results as $r) {
  		$types[ $r->cvterm_id ] = substr($r->name,0,35);
  	}
  	$results = tripal_core_chado_select('cvterm',array('cvterm_id','name'),array('cv_id'=>array('name'=>'tripal')));
  	foreach ($results as $r) {
  		$types[ $r->cvterm_id ] = substr($r->name,0,35);
  	}
  	$form['featuredfn']['relationship_type_id'] = array(
  		'#title' => 'Locus => Marker Relationship Type',
  		'#type' => 'select',
  		'#options' => $types,
  		'#description' => 'The type of relationship linking a Locus to a Marker.',
  		'#default_value' => $this->options['featuredfn']['relationship_type_id'],
  	);

  	// Sets which part of the relationship (subject or object) is the locus
  	$form['featuredfn']['locus_pos'] = array(
  		'#title' => 'Locus is',
  		'#type' => 'radios',
  		'#options' => array(
  		  'subject_id' => 'Subject (ie: [Current Stock] is_locus_of [Marker])',
  		  'object_id' => 'Object (ie: [Locus] is_locus_of [Current Stock]'
  		),
  		'#default_value' => $this->options['featuredfn']['locus_pos'],
  	);

  	// Allows the user to add any flanking SNPs to the FASTA export as their IUPAC codes
    $form['add_flanking_snps'] = array(
      '#type' => 'checkbox',
      '#title' => 'Pull in genotypes of flanking loci/markers',
      '#description' => 'If this checkbox is checked then the genotypes of flanking loci/SNPs '
           .'(flanking loci are those aligned to the same backbone) will appear in '
         .'the FASTA as the IUPAC code.'
    );
  }

  /**
   * The main function used to render the entire export file
   */
  function render_body() {

    // Some error checking
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }

    if (!isset($this->options['feature_column'])) {
      drupal_set_message('Please set the feature ID column in the Style Options', 'error');
      return '';
    }

    if ($this->options['featuredfn']['feature_type'] == 'contig') {
      drupal_set_message('The Kaspar Export Plugin is unable to determine which loci the '
              .'assay should be designed for in this case. Please attach this export to a view with '
              .'either loci or marker features', 'error');
      return '';
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
    foreach ($sets as $title => $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows = $records;
      }

      // Render each row which will become each FASTA record
      $other_feature = array(
        'subject_id' => 'object_id',
        'object_id' => 'subject_id'
      );
      $this->other_feature = $other_feature;
      foreach ($rows as $r) {

        $sequence_feature_id = $r->{$this->options['feature_column']};
        $options['original_feature_id'] = $r->{$this->options['feature_column']};

        // Look-up any additional features needed depending on the type of features we have
        $skip = FALSE;
        switch ($this->options['featuredfn']['feature_type']) {
          case 'locus':

            // Locus (is current feature)
            $options['locus_feature_id'] = $options['original_feature_id'];

            $sql = "SELECT value FROM featureprop WHERE feature_id=%d AND type_id IN (SELECT cvterm_id FROM cvterm WHERE name='%s')";
            $flanking_5 = db_fetch_object(db_query($sql, $options['locus_feature_id'], 'five_prime_flanking_region'));
            $flanking_3 = db_fetch_object(db_query($sql, $options['locus_feature_id'], 'three_prime_flanking_region'));
            $options['locus_5prime_flanking'] = $flanking_5->value;
            $options['locus_3prime_flanking'] = $flanking_3->value;

            // Parent (current feature aligned to it)
            $has_sequence = db_fetch_object(db_query('SELECT true as boolean FROM feature WHERE feature_id=%d AND residues IS NOT NULL',$options['locus_feature_id']));
            if ($has_sequence->boolean) {
              $options['is_parent'] = FALSE;
              $options['parent'] = FALSE;
              $options['contig_feature_id'] = NULL;
            } else {
              // check if it's aligned on another feature that does have sequence
              // and output a fasta record for the parent feature
              $parent = db_fetch_object(db_query('SELECT * FROM featureloc WHERE feature_id=%d ORDER BY locgroup ASC, rank ASC LIMIT 1',$options['locus_feature_id']));
              if (isset($parent->srcfeature_id)) {
                $parent_has_seq = db_fetch_object(db_query('SELECT true as boolean FROM feature WHERE feature_id=%d AND residues IS NOT NULL',$parent->srcfeature_id));
                if ($parent_has_seq->boolean) {
                  $options['is_parent'] = TRUE;
                  $options['parent'] = $parent;
                  $options['contig_feature_id'] = $parent->srcfeature_id;
                  $sequence_feature_id = $options['contig_feature_id'];
                } else {
                  $skip = TRUE;
                  break;
                }
              }
            }

            // Marker (related to current feature via relationship type)
            $sql = "SELECT %s as marker_id FROM feature_relationship WHERE %s=%d AND type_id=%d";
            $marker = db_fetch_object(db_query($sql,
              $this->options['featuredfn']['locus_pos'],
              $other_feature[$this->options['featuredfn']['locus_pos']],
              $options['locus_feature_id'],
              $this->options['featuredfn']['relationship_type_id']
            ));
            if ($marker->marker_id) {
              $options['marker_feature_id'] = $marker->marker_id;
            } else {
              $skip = TRUE;
              break;
            }

          break;
          case 'marker':
            $options['marker_feature_id'] = $options['original_feature_id'];

            $sql = "SELECT %s as locus_id FROM feature_relationship WHERE %s=%d AND type_id=%d";
            $locus = db_fetch_object(db_query($sql,
              $this->options['featuredfn']['locus_pos'],
              $other_feature[$this->options['featuredfn']['locus_pos']],
              $options['marker_feature_id'],
              $this->options['featuredfn']['relationship_type_id']
            ));
            if ($locus->locus_id) {
              $options['locus_feature_id'] = $locus->locus_id;
            } else {
              $skip = TRUE;
              break;
            }

            $sql = "SELECT value FROM featureprop WHERE feature_id=%d AND type_id IN (SELECT cvterm_id FROM cvterm WHERE name='%s')";
            $flanking_5 = db_fetch_object(db_query($sql, $options['locus_feature_id'], 'five_prime_flanking_region'));
            $flanking_3 = db_fetch_object(db_query($sql, $options['locus_feature_id'], 'three_prime_flanking_region'));
            $options['locus_5prime_flanking'] = $flanking_5->value;
            $options['locus_3prime_flanking'] = $flanking_3->value;

            // Parent (current feature aligned to it)
            $has_sequence = db_fetch_object(db_query('SELECT true as boolean FROM feature WHERE feature_id=%d AND residues IS NOT NULL',$options['locus_feature_id']));
            if ($has_sequence->boolean) {
              $options['is_parent'] = FALSE;
              $options['parent'] = FALSE;
              $options['contig_feature_id'] = NULL;
            } else {
              // check if it's aligned on another feature that does have sequence
              // and output a fasta record for the parent feature
              $parent = db_fetch_object(db_query('SELECT * FROM featureloc WHERE feature_id=%d ORDER BY locgroup ASC, rank ASC LIMIT 1',$options['locus_feature_id']));
              if (isset($parent->srcfeature_id)) {
                $parent_has_seq = db_fetch_object(db_query('SELECT true as boolean FROM feature WHERE feature_id=%d AND residues IS NOT NULL',$parent->srcfeature_id));
                if ($parent_has_seq->boolean) {
                  $options['is_parent'] = TRUE;
                  $options['parent'] = $parent;
                  $options['contig_feature_id'] = $parent->srcfeature_id;
                  $sequence_feature_id = $options['contig_feature_id'];
                } else {
                  $skip = TRUE;
                  break;
                }
              }
            }

          break;
        }

        // Actually render the FASTA record using previous information looked-up
        if (!$skip) {
          // Uses the parent class render_fasta_header function to render the fasta header
          $fasta_header = $this->render_fasta_header($sequence_feature_id, $r, $options);
          // Render the FASTA sequence
          $fasta_seq = $this->render_fasta_sequence($sequence_feature_id, $r, $options);
          if ($fasta_seq) {
            $output .= $fasta_header . "\n" . $fasta_seq . "\n";
          }
        }

      }
    }
    unset($this->view->row_index);
    return $output;

  }

  /**
   * Render the Sequence portion of the FASTA record supplied as parameters
   */
  function render_fasta_sequence ($feature_id, $values, $options) {

    // If flanking is composed of sequence, use that
    if (preg_match('/[ATGCatgc]+/',$options['locus_5prime_flanking'])) {

      $alleles = $this->retieve_alleles_for_marker($options['marker_feature_id']);
      if ($alleles) {
        // If the admin has indicated, add flanking SNPs
        if ($this->options['add_flanking_snps']) {


          $str = array_merge(str_split($options['locus_5prime_flanking']), array('['.implode('/',$alleles).']'), str_split($options['locus_3prime_flanking']));

          $seg_start = $options['parent']->fmin - strlen($options['locus_5prime_flanking']);
          $seg_end = $options['parent']->fmin + strlen($options['locus_3prime_flanking']);
          $str = $this->add_flanking_genotypes_to_segment($str, $feature_id, $seg_start, $seg_end, $options['parent']->fmin-$seg_start);

          $sequence = implode('', $str);
        } else {
          $alleles = $this->retieve_alleles_for_marker($options['marker_feature_id']);
          $sequence = $options['locus_5prime_flanking'] . '['.implode('/',$alleles).']' . $options['locus_3prime_flanking'];
        }
      }

    // Use the sequence from the feature
    } else {
      $sql = 'SELECT residues FROM feature WHERE feature_id=%d';
      $previous_db = tripal_db_set_active('chado');
      $r = db_fetch_object(db_query($sql,$feature_id));
      tripal_db_set_active($previous_db);

      $sequence  = $r->residues;

      // if the sequence is from the parent of the current feature
      // markup the sequence to show where the feature is
      if ($options['is_parent']) {
        $str = str_split($sequence);

        // All Alleles for this locus
        $alleles = $this->retieve_alleles_for_marker($options['marker_feature_id']);
        $start = $options['parent']->fmin+1;
        $str[$start] = '['.implode('/',$alleles).']';



        // if the amount of flanking sequence is specified
        // use this to trim the parent sequence
        if (preg_match('/(\d+)\s*bp/',$options['locus_5prime_flanking'],$matches)) {
          $flanking5 = $matches[1];
        }
        if (preg_match('/(\d+)\s*bp/',$options['locus_3prime_flanking'],$matches)) {
          $flanking3 = $matches[1];
        }
        if ($flanking5 && $flanking3) {
          $first2keep = $start - $flanking5 -1;
          $length2keep = $flanking5 + 1 + $flanking3;
          $str = array_slice($str, $first2keep, $length2keep);
        } elseif ($flanking5) {
          $str = array_slice($str, $first2keep);
        } elseif ($flanking3) {
          $length2keep = 0 - $flanking3;
          $str = array_slice($str, 0, $length2keep);
        }


        //add flanking SNPs
        if ($this->options['add_flanking_snps']) {
          $seg_start = $first2keep;
          $seg_end = $first2keep + $length2keep;
          $str = $this->add_flanking_genotypes_to_segment($str, $feature_id, $seg_start, $seg_end, $options['parent']->fmin-$seg_start);
        }

        $sequence  = implode('',$str);
      }
    }

    if ($sequence) {
      return wordwrap($sequence, 80, "\n", TRUE);
    } else {
      return FALSE;
    }
  }

  /**
   * Add any flanking genotypes to the sequence supplied as a parameter
   *
   * @return
   *  sequence supplied with flanking genotypes added
   */
  function add_flanking_genotypes_to_segment($seq, $sequence_feature_id, $seg_start, $seg_end, $main_allele_loc) {

    // get all loci aligned to sequence feature
    $sql = "SELECT feature_id, fmin, fmax FROM featureloc WHERE srcfeature_id=%d AND fmin >= %d AND fmax <= %d";
    $resource = db_query($sql, $sequence_feature_id, $seg_start, $seg_end);
    while ( $r = db_fetch_object($resource) ) {
      // location of loci to input genotypes for
      $seg_loc = $r->fmin - $seg_start;

      if ($seg_loc != $main_allele_loc) {
        // get genotypes
        $sql = "SELECT %s as marker_id FROM feature_relationship WHERE %s=%d AND type_id=%d";
        $marker = db_fetch_object(db_query($sql,
          $this->other_feature[$this->options['featuredfn']['locus_pos']],
          $this->options['featuredfn']['locus_pos'],
          $r->feature_id,
          $this->options['featuredfn']['relationship_type_id']
        ));
        $alleles = $this->retieve_alleles_for_marker($marker->marker_id);

        // input genotypes to string at segment location
        $code = $this->IUPAC[ implode ('',$alleles) ];
        if ($code) {
          $seq[$seg_loc] = $code;
        } else {
          $seq[$seg_loc] = 'N';
        }
      }

    }

    return $seq;
  }

  /**
   * Retrives the genotypes for a given marker
   * Assumes they are stored in the chado genotype table
   *
   * @param $feature_id
   *  The chado.feature_id of the marker the genotypes are associated with
   * @return
   *  An array of alleles
   */
  function retieve_alleles_for_marker ($feature_id) {

    $sql = "SELECT distinct(substring(description, '^([ATGCatcg]).*')) as allele FROM genotype "
      ."WHERE genotype_id IN (SELECT genotype_id FROM feature_genotype "
      ."WHERE feature_id=%d) AND description~'^[ATGCatcg]'";
    $resource = db_query($sql, $feature_id);

    $alleles = array();
    while ($r = db_fetch_object($resource)) {
      $alleles[] = $r->allele;
    }

    return $alleles;
  }

}