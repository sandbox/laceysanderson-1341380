<?php
/**
 * @file
 * Plugin include file for export style plugin.
 */

/**
 * Generalized style plugin for export plugins.
 *
 * @ingroup views_style_plugins
 */
class views_data_export_plugin_style_export_fasta extends views_data_export_plugin_style_export {

  function init (&$view, $options) {
    parent::init($view, $options);

    $tokens = array();
    
    // Tokens for the fasta feature
    $tokens[t('Fasta Feature')]['%fasta_feature_id'] = array(
      'defn' => t('feature unique identifier'),
      'table' => 'feature',
      'field' => 'feature_id'
    );
    $tokens[t('Fasta Feature')]['%fasta_feature_name'] = array(
      'defn' => t('the human-readable name of the feature'),
      'table' => 'feature',
      'field' => 'name'
    );
    $tokens[t('Fasta Feature')]['%fasta_feature_uniquename'] = array(
      'defn' => t('the unique name of the feature'),
      'table' => 'feature',
      'field' => 'uniquename'
    );
    $tokens[t('Fasta Feature')]['%fasta_feature_seq_length'] = array(
      'defn' => t('the length of the sequence'),
      'table' => 'feature',
      'field' => 'seq_len'
    );
    $tokens[t('Fasta Feature')]['%fasta_organism_common_name'] = array(
      'defn' => t('common name of the organism the feature belongs to'),
      'table' => 'organism',
      'field' => 'common_name'
    );
    $tokens[t('Fasta Feature')]['%fasta_organism_genus'] = array(
      'defn' => t('genus of the organism the feature belongs to'),
      'table' => 'organism',
      'field' => 'genus'
    );
    $tokens[t('Fasta Feature')]['%fasta_organism_species'] = array(
      'defn' => t('species of the organism the feature belongs to'),
      'table' => 'organism',
      'field' => 'species'
    ); 
    $tokens[t('Fasta Feature')]['%fasta_feature_type'] = array(
      'defn' => t('the type of feature'),
      'table' => 'cvterm',
      'field' => 'name'
    );
    
    // Tokens for the feature indicated by the view
    // different if feature doesn't have sequence but parent does
    // this is the feature and fasta feature is the parent
    // Tokens for the fasta feature
    $tokens[t('View Feature')]['%view_feature_id'] = array(
      'defn' => t('feature unique identifier'),
      'table' => 'feature',
      'field' => 'feature_id'
    );
    $tokens[t('View Feature')]['%view_feature_name'] = array(
      'defn' => t('the human-readable name of the feature'),
      'table' => 'feature',
      'field' => 'name'
    );
    $tokens[t('View Feature')]['%view_feature_uniquename'] = array(
      'defn' => t('the unique name of the feature'),
      'table' => 'feature',
      'field' => 'uniquename'
    );
    $tokens[t('View Feature')]['%view_feature_seq_length'] = array(
      'defn' => t('the length of the sequence'),
      'table' => 'feature',
      'field' => 'seq_len'
    );
    $tokens[t('View Feature')]['%view_organism_common_name'] = array(
      'defn' => t('common name of the organism the feature belongs to'),
      'table' => 'organism',
      'field' => 'common_name'
    );
    $tokens[t('View Feature')]['%view_organism_genus'] = array(
      'defn' => t('genus of the organism the feature belongs to'),
      'table' => 'organism',
      'field' => 'genus'
    );
    $tokens[t('View Feature')]['%view_organism_species'] = array(
      'defn' => t('species of the organism the feature belongs to'),
      'table' => 'organism',
      'field' => 'species'
    ); 
    $tokens[t('View Feature')]['%view_feature_type'] = array(
      'defn' => t('the type of feature'),
      'table' => 'cvterm',
      'field' => 'name'
    );
    
    $tokens[t('Parent-Child Details')]['%seq_is_parent'] = array(
      'defn' => 'Yes/No depnding on whether the sequence is from the parent of view feature',
    );

    $tokens[t('Parent-Child Details')]['%child_location'] = array(
      'defn' => 'the coordinates of the child/view feature on the parent',
    );
    
    $this->definition['fasta_header_tokens'] = $tokens;
  }
  
  /**
   * Set options fields and default values.
   *
   * @return
   * An array of options information.
   */
  function option_definition() {
    $options = parent::option_definition();
    
    $options['feature_column'] = array(
      'default' => 0,
      'translatable' => FALSE,
    );
    
    return $options;
  }
  
  /**
   * Options form mini callback.
   *
   * @param $form
   * Form array to add additional fields to.
   * @param $form_state
   * State of the form.
   * @return
   * None.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $options = array();
    foreach ($this->view->display['default']->display_options['fields'] as $k => $f) {
      if ($f['field'] == 'feature_id') {
        $options[$k] = $f['label'];
      }
    }
    $form['feature_column'] = array(
      '#type' => 'select',
      '#title' => 'Feature ID Column',
      '#description' => 'The column/field that contains the name of the feature to download the fasta for',
      '#options' => $options,
      '#default_value' => $this->options['feature_column'],
    );
    
    $form['fasta_header'] = array(
      '#type' => 'textfield',
      '#title' => 'Fasta Header',
      '#maxlength' => 250,
      '#description' => 'The text (including replacement tokens below) to use for the fasta header of each fasta record output.',
      '#default_value' => $this->options['fasta_header'],
    );

    // General token replacement.
    $output = t('<p>The following substitution patterns are available for this display. Use the pattern shown on the left to display the value indicated on the right.</p>');
    $output .= t('<p>The Fasta Feature is specifically the chado feature that the sequence output is from. The View Feature is the feature indicated by the Feature ID column selected. In the case where a view feature doesn\'t have sequence but is aligned on a parent feature with sequence, the fasta feature is the parent feature.</p>');
    
    // Get a list of the available arguments for token replacement.
    $options = array();
    $tokens = $this->definition['fasta_header_tokens'];
    foreach ($tokens as $type => $type_tokens) {
      foreach ($type_tokens as $token => $v) {
        $options[$type][$token] = $v['defn'];
      }
    }
    

    // We have some options, so make a list.
    if (!empty($options)) {
      foreach (array_keys($options) as $type) {
        if (!empty($options[$type])) {
          $items = array();
          foreach ($options[$type] as $key => $value) {
            if ($key == 'description') {
              $items[] = $value;
            } else {
              $items[] = $key . ' == ' . $value;
            }
          }
          $output .= theme('item_list', $items, $type);
        }
      }
    }
    // This construct uses 'hidden' and not markup because process doesn't
    // run. It also has an extra div because the dependency wants to hide
    // the parent in situations like this, so we need a second div to
    // make this work.
    $form['header_tokens'] = array(
      '#type' => 'item',
      '#value' => '<div><fieldset id="views-tokens-header"><legend>' . t('Replacement patterns') . '</legend>' . $output . '</fieldset></div>',
    );
  }
  
  function render_header() {
    $output = '';
    return $output;
  }

  function render_footer() {
    $output = '';
    return $output;
  }

  function render_body() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }

    if (!isset($this->options['feature_column'])) {
      drupal_set_message('Please set the feature ID column in the Style Options', 'error');
      return '';
    }
    
    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
    foreach ($sets as $title => $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows = $records;
      }

      // Render each row
      foreach ($rows as $r) {
        
        $feature_id = $r->{$this->options['feature_column']};
        $is_parent = FALSE;
        
        // if feature doesn't have sequence
        $has_sequence = db_fetch_object(db_query('SELECT true as boolean FROM feature WHERE feature_id=%d AND residues IS NOT NULL',$feature_id));
        if (!$has_sequence->boolean) {
          // check if it's aligned on another feature that does have sequence
          // and output a fasta record for the parent feature
          $parent = db_fetch_object(db_query('SELECT * FROM featureloc WHERE feature_id=%d ORDER BY locgroup ASC, rank ASC LIMIT 1',$feature_id));
          if (isset($parent->srcfeature_id)) {
            $parent_has_seq = db_fetch_object(db_query('SELECT true as boolean FROM feature WHERE feature_id=%d AND residues IS NOT NULL',$parent->srcfeature_id));
            if ($parent_has_seq->boolean) {
              $feature_id = $parent->srcfeature_id;
              $is_parent = TRUE;
            } else {
              continue;
            }
          }
        }
        
        // render fasta record
        $options = array(
          'is_parent' => $is_parent,
          'parent' => $parent,
          'original_feature_id' => $r->{$this->options['feature_column']}
        );
        $fasta_header = $this->render_fasta_header($feature_id, $r, $options);
        $fasta_seq = $this->render_fasta_sequence($feature_id, $r, $options);
        if ($fasta_seq) {
          $output .= $fasta_header . "\n" . $fasta_seq . "\n";
        }
      }
    }
    unset($this->view->row_index);
    return $output;

  }
  
  function render_fasta_header ($feature_id, $values, $options) {
    
    $tokens = $this->replace_fasta_header_tokens($feature_id, $values, $options);
    $header = '>' . strtr($this->options['fasta_header'], $tokens);
    
    return $header;
  }
  
  function replace_fasta_header_tokens ($feature_id, $values, $options) {
    $tokens = array();
    
    // Fasta Feature Tokens
    $feature = db_fetch_object(db_query('SELECT * FROM feature WHERE feature_id=%d',$feature_id));
    $organism = db_fetch_object(db_query('SELECT * FROM organism WHERE organism_id=%d',$feature->organism_id));
    $cvterm = db_fetch_object(db_query('SELECT * FROM cvterm WHERE cvterm_id=%d',$feature->type_id));

    foreach ($this->definition['fasta_header_tokens'][t('Fasta Feature')] as $token => $v) {
      $tokens[$token] = $$v['table']->{$v['field']};
    }
    
    // View Feature Tokens
    if ($options['is_parent']) {
      $feature = db_fetch_object(db_query('SELECT * FROM feature WHERE feature_id=%d',$options['original_feature_id']));
      $organism = db_fetch_object(db_query('SELECT * FROM organism WHERE organism_id=%d',$feature->organism_id));
      $cvterm = db_fetch_object(db_query('SELECT * FROM cvterm WHERE cvterm_id=%d',$feature->type_id));      
    }

    foreach ($this->definition['fasta_header_tokens'][t('View Feature')] as $token => $v) {
      $tokens[$token] = $$v['table']->{$v['field']};
    }
    
    // Parent-Child Detail Tokens
    if ($options['is_parent']) {
      foreach ($this->definition['fasta_header_tokens'][t('Parent-Child Details')] as $token => $v) {
        switch ($token) {
          case '%seq_is_parent':
            $tokens[$token] = 'Yes';
          break;
          case '%child_location':
            $tokens[$token] = $options['parent']->fmin .'-' . $options['parent']->fmax;
          break;
          default:
            $tokens[$token] = '';
          break;
        }
      }
    } else {
      foreach ($this->definition['fasta_header_tokens'][t('Parent-Child Details')] as $token => $v) {
        switch ($token) {
          case '%seq_is_parent':
            $tokens[$token] = 'No';
          break;
          case '%child_location':
            $tokens[$token] = 'Full Sequence';
          break;
          default:
            $tokens[$token] = '';
          break;
        }
      }    
    }
    
    return $tokens;
  }
  
  function render_fasta_sequence ($feature_id, $values, $options) {
    
    $sql = 'SELECT residues FROM feature WHERE feature_id=%d';
    $previous_db = tripal_db_set_active('chado');
    $r = db_fetch_object(db_query($sql,$feature_id));
    tripal_db_set_active($previous_db);
    
    $sequence = $r->residues;
    
    // if the sequence is from the parent of the current feature
    // markup the sequence to show where the feature is
    if ($options['is_parent']) {
      $str = str_split(strtolower($sequence));
      $start = $options['parent']->fmin+1;
      $end = $options['parent']->fmax;
      for ($i = $start; $i <= $end; $i++) {
        $str[$i] = strtoupper($str[$i]);
      }
      $sequence = implode('',$str);
    }
    
    if ($sequence) {
      return wordwrap($sequence, 80, "\n", TRUE);
    } else {
      return FALSE;
    }
  }
}